{$I defines.inc}
uses Dos,Clanky,lacrt;

function MyVal(s:string):longint;
var i,j:longint;
begin
Val(s,i,j);
MyVal:=i;
end;


Function KontrolaPredanychParametru(var segm,ofse:longint):boolean;
var k,l,m:longint;
    s,par:string;
    dpar,bpar:string;
    c,d:char;
    veslove:boolean;

begin

{for k:=1 to Envcount do writeln(envstr(k));
readln;}

s:=GetEnv('VENOMLINK');
if s='' then
   begin
   writeln('This program is not a standalone application but plugin for Blocek.');
   Exit(false);
   end;

dpar:='';
bpar:='';
veslove:=false;
s:=s+' ';
segm:=0;
ofse:=0;
for k:=1 to Length(s) do
    begin
    c:=s[k];
    if c<>' ' then
       if veslove=false then
          begin
          veslove:=true;
          l:=k;
          end else
       else   {tzn. c=' '}
       if veslove=true then
          begin
          veslove:=false;
          par:=Copy(s,l,k-l);
          d:=UpCase(par[1]);
          m:=MyVal(Copy(par,2,255));
          if d='D' then
             if m<>0 then segm:=m else Exit(false) else

          if d='B' then
             if m<>0 then ofse:=m else Exit(false);
          end;

    end;

if (segm=0) or (ofse=0) then Exit(false);
KontrolaPredanychParametru:=true;
end;



Function Zjisti_delku_predavaneho_bloku(segm,ofse:longint):longint;assembler;
asm
push ds
mov eax,segm
mov ds,ax
mov ebx,ofse
mov eax,ds:[ebx]
pop ds
end;


Procedure Nacti_data_z_predavaneho_bloku(segm,ofse,vel_bloku:longint;predavany_blok:pchar);
begin
asm
push es
push edi
mov eax,segm
mov es,ax
mov ebx,ofse
mov ecx,vel_bloku
mov edi,predavany_blok

@smycka:
mov al,es:[ebx]
mov ds:[edi],al
inc ebx
inc edi
Loop @smycka

pop edi
pop es
end;
end;


Function get_far_pointer(segm:word;hl:pdpole;i:longint):pointer;assembler;
asm
push esi
push es
mov ax,segm
mov es,ax
mov esi,hl
mov ebx,i
shl ebx,2 {EBX:=EBX*4}
add esi,ebx
mov eax,es:[esi]
pop es
pop esi
end;



var segm,ofse:longint;
    predavany_blok:pchar;
    delka_bloku:longint;
    ofse_hlasky:^longint;
    oh:pointer;
    typ:^longint;
    i:longint;
    p:Pclanek;


begin


if not KontrolaPredanychParametru(segm,ofse) then Halt(255);

delka_bloku:=Zjisti_delku_predavaneho_bloku(segm,ofse);
if delka_bloku<=0 then Halt(254);
GetMem(predavany_blok,delka_bloku);
Nacti_data_z_predavaneho_bloku(segm,ofse,delka_bloku,predavany_blok);

ofse_hlasky:=@predavany_blok[8];
typ:=@predavany_blok[4];

oh:=pointer(ofse_hlasky^);

writeln(far_hlla(segm,oh,'zatim_neni'));

readln;
end.
