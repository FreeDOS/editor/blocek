Program disktest;
uses disk,dos,crt,lacrt{,ide} {... kdyz odkomentuju, tak automaticky bude
     vyuzivat sluzeb jednotky IDE};


type TExtdskFuncPack = packed record
    xfSize: Byte;
    xfReserved: Byte;
    xfSectorNum: Word;
    xfDataBuffer_ofs:word;
    xfDataBuffer_seg:word;
    xfLBASector: packed array[0..1] of dword;
  end;

var _ax,_bx,_cx,_dx,_ds,_si:word;
    _precteno:boolean;
    zadanka:TExtdskFuncPack;

const HexaNum : string[16] = '0123456789ABCDEF';


function HexaStr(D:Dword;L:Byte):string;
var I:Byte;
    S:string;
begin
S:='';
for I:=L-1 downto 0 do S:=S+HexaNum[(D shr (I*4) and $0F) + 1];
HexaStr:=S;
end;

Function JePrimarni(odl:TOddInfo):boolean;
begin
if odl.di^.LBA
   then JePrimarni:=odl.parentsec.lba=0
   else JePrimarni:=(odl.parentsec.cyl=0) and (odl.parentsec.hla=0) and (odl.parentsec.sek=1);
end;


Procedure TstDOS;
var f:file;
    p:array[0..5110] of byte;
    r:byte;
begin
assign(f,'disktest.tst');
rewrite(f,1);
fillchar(p,512,0);
r:=0;
Nacti_Sektory_pres_DOS('c',0,1,true,r,p);
blockwrite(f,p,512);
close(f);
end;


var a:byte;
    b:byte;
    c:byte;
    d:char;
    ff,fff,nf,nff:byte;
    bd:TBIOSdisk;
    ft:TFAT_Driver;
    nt:TNTFS_Driver;
    v:longint;

    idep:word;
    idem:boolean;

begin
{DOS_disk_na_BIOS_disk('C');}

writeln('Velikost sektoru na disku C: je ',Zjisti_BajtSekt_pres_DOS('c'));
{readln;}


writeln(#13#10#13#10);
writeln('Program proscanuje tvuj pocitac a bude hledat diskety, pevne disky a'#13#10'oddily na nich.');
writeln('Pozor - jestlize je rozhrani disku nasavene na SCSI bez rezimu kompatibility,');
writeln('tak nemusi fungovat korektne.');
writeln;


bd.init;
writeln(bd.disk[2].ZjistiTypDisku);

tstDOS;  halt(0);
writeln('Cteni bootsektoru pres funkce DOSu probehlo.');

readln;

Writeln('Pocet disketovych mechanik: ',bd.disketovek);
writeln('pocet pevnych a USB disku: ',bd.disknum-bd.disketovek);
writeln('pocet pristupnych oddilu (bez disketovek): ',bd.oddlnum-bd.disketovek);
writeln;

d:='D';
{}a:=MapovaniDiskuDOSu(d,bd);
writeln('Jednotka ',d,': se mapuje do zaznamu c.',a);{}
if a<>0 then
   begin
   writeln('Ten je na disku ',HexaStr(bd.oddl[a].di^.disk,2),'h, jehoz modelove oznaceni je: '
           ,bd.oddl[a].di^.ZjistiTypDisku);
   writeln('A jeho seriove cislo je: ',bd.oddl[a].di^.ZjistiSerioveCislo);
   end;

readln;

b:=1;
ff:=0;
nf:=0;
for a:=1 to bd.disknum do
    begin
    write('Disk c.',HexaStr(bd.disk[a].disk,2),'h',
          ' (',bd.disk[a].Dej_velikost_citelne,')',
          ' ma ',bd.disk[a].oddilu,
          ' oddilu');

    if a<=bd.disketovek
       then write(' (disketa)'#13#10#13#10)
       else begin
       writeln(':');
       for c:=b to b+bd.disk[a].oddilu-1 do
           begin
           write('oddil ',c-b+1,' o velikosti ',bd.oddl[c].Dej_velikost_citelne,' ');
           write('kod systemu: ',HexaStr(bd.oddl[c].kodFS,2),'h ');
           write('popiska: ',bd.oddl[c].PrectiPopisku,#13#10);
           if (ff=0) and (bd.oddl[c].ZjistitypFAT<>0) then begin ff:=c;fff:=c-b+1;end;
           if (nf=0) and (bd.oddl[c].NTFS_exFAT_jina=1) then begin nf:=c;nff:=c-b+1;end;
           end;
       writeln;
       b:=c;
       end;
    inc(b);
    end;
writeln('Zmackni Enter');
{readln;}

if ff<>0 then
   begin
   writeln('Prvni FAT oddil jsem nalezl na disku ',
            HexaStr(bd.oddl[ff].di^.disk,2),'h, na jeho ',fff,'. oddilu');
   if JePrimarni(bd.oddl[ff]) then writeln('(jde o primarni oddil)')
                             else writeln('(jde o logicky oddil)');

   bd.oddl[ff].UlozBootSectorDoSouboru('disktest.tsu');

   ft.Init(bd.oddl[ff]);
   writeln('Zkoumam FAT zaznam:');
   writeln('Seriove cislo oddilu: ',ft.Vrat_SerioveCislo);
   writeln('Jmenovka oddilu: ',ft.Vrat_VolumeLabel);
   end;

writeln;

if nf<>0 then
   begin
   writeln('Prvni NTFS oddil jsem nalezl na disku ',
            HexaStr(bd.oddl[c].di^.disk,2),'h, na jeho ',nff,'. oddilu');
   if JePrimarni(bd.oddl[c]) then writeln('(jde o primarni oddil)')
                             else writeln('(jde o logicky oddil)');
   nt.Init(bd.oddl[nf]);
   writeln('Zkoumam NTFS zaznam:');
   writeln('Verejne seriove cislo oddilu: ',nt.Vrat_SerioveCislo);
   writeln('Uplne seriove cislo oddilu: ',nt.Vrat_UplneSerioveCislo);
   end;

{readln;}
end.
