Procedure Dekoduj_z_B64(kbuf,dbuf:pchar;kbuf_vel:longint);
{kbuf i dbuf uz musi byt alokovane a kbuf_vel tedy musi byt znama}
const
na = 255;
pc = 254;
b64DecTable: array[0..255] of Byte =
    (NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
     NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
     NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 62, NA, NA, NA, 63,
     52, 53, 54, 55, 56, 57, 58, 59, 60, 61, NA, NA, NA, PC, NA, NA,
     NA, 00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14,
     15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, NA, NA, NA, NA, NA,
     NA, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
     41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, NA, NA, NA, NA, NA,
     NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
     NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
     NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
     NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
     NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
     NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
     NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
     NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA);


var i,j,m,n:longint;
    fin:array[0..2] of char;
begin
m:=0;

if kbuf_vel>0 then
   if kbuf[kbuf_vel-1]='=' then
      if (kbuf_vel>1) and (kbuf[kbuf_vel-2]='=') then m:=2 else m:=1 else m:=0;

i:=0;
j:=0;

if m=0 then n:=kbuf_vel else n:=kbuf_vel-3;

while i<n do
   begin
   dbuf[j+0]:=char(b64dectable[byte(kbuf[i+0])] shl 2 or (b64dectable[byte(kbuf[i+1])]) shr 4);
   dbuf[j+1]:=char(b64dectable[byte(kbuf[i+1])] and 15 shl 4 or (b64dectable[byte(kbuf[i+2])]) shr 2);
   dbuf[j+2]:=char(b64dectable[byte(kbuf[i+2])] and 3 shl 6 or (b64dectable[byte(kbuf[i+3])]));
   inc(j,3);
   inc(i,4);
   end;

if m>0 then
   begin
   fin[0]:=char(b64dectable[byte(kbuf[i+0])] shl 2 or (b64dectable[byte(kbuf[i+1])]) shr 4   );
   fin[1]:=char(b64dectable[byte(kbuf[i+1])] and 15 shl 4 or (b64dectable[byte(kbuf[i+2])]) shr 2);
   {fin[2]:=char(dectable[byte(kbuf[i+2])] and 3 shl 6 or (dectable[byte(kbuf[i+3])]));}
   dbuf[j+0]:=fin[0];
   if m=2 then dbuf[j+1]:=fin[1];
   end;
end;


Function Kopiruj_z_b64(buf64:pchar;var BufDKD:pointer):longint;
{buf64 jsou nactena data v Base64 (predpoklada se, ze validni) a BufDKD je
 buffer, ktery teprve bude alokovan. Vysledkem funkce je delka bufferu.}
{}function Plength(p:pchar):longint;
{}var i:longint;
{}begin
{}i:=0;
{}while p[i]<>#0 do inc(i);
{}PLength:=i;
{}end;

var i,j:longint;
begin
i:=PLength(buf64);
j:=i*3 div 4;
if i>0 then
   if buf64[i-1]='=' then
      if (i>1) and (buf64[i-2]='=') then dec(j,2) else dec(j,1);

GetMem(BufDKD,j);
Dekoduj_z_B64(buf64,BufDKD,i);
Kopiruj_z_b64:=j;
end;


var tst:pchar='TGFhY2Ey';
    bb,cc:pchar;
    i:longint;
begin
i:=Kopiruj_z_b64(tst,bb);

GetMem(cc,i+1);
Move(bb^,cc^,i);
cc[i]:=#0;

writeln(tst);
writeln('*'+cc+'*');
end.