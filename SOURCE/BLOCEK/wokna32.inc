const
cp852tbl:utf8conv =
  (199,252,233,226,228,367,263,231,322,235,336,337,238,377,196,262,201,313,314,244,246,317,318,346,347,214,220,356,357,321,
   215,269,225,237,243,250,260,261,381,382,280,281,65535,378,268,351,171,187,9617,9618,9619,9474,9508,193,194,282,350,9571,9553,9559,
   9565,379,380,9488,9492,9524,9516,9500,9472,9532,258,259,9562,9556,9577,9574,9568,9552,9580,164,273,272,270,203,271,327,205,206,283,9496,
   9484,9608,9604,354,366,9600,211,223,212,323,324,328,352,353,340,218,341,368,253,221,355,180,173,733,731,711,728,167,247,184,
   176,168,729,369,344,345,9632,160);

_lsipka_obrazek:array[1..225] of word =
($FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF);

_psipka_obrazek:array[1..225] of word =
($FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF);

_hsipka_obrazek:array[1..225] of word =
($FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,
$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,
$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,
$0,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$0,$0,
$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,$0,$0,$0,$0,$0,$0,$0,$0,$0,
$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF);

_dsipka_obrazek:array[1..225] of word =
($FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$0,$0,$0,$0,$0,$0,$0,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$0,$0,$0,$0,$0,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$0,
$0,$0,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$0,$0,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$0,$0,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$0,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,$FFFF,
$FFFF);


Procedure NastavHlasky;
begin
dvojlistbox_vsepol:=w_VSEPOL;
dvojlistbox_vybpol:=w_VYBPOL;
end;

Procedure NactiHlasky_ze_souboru(s:string);
begin
if not ExistFile(s) then Exit;
w32_hlasky:=Nacti_Data(s);
w_OK := hllp(w32_hlasky,'_OK');
w_OK2 := hllp(w32_hlasky,'_OK2');
w_ANO := hllp(w32_hlasky,'_ANO');
w_NE := hllp(w32_hlasky,'_NE');
w_CANCEL := hllp(w32_hlasky,'_CANCEL');
w_CANCEL2 := hllp(w32_hlasky,'_CANCEL2');
w_POMOC := hllp(w32_hlasky,'_POMOC');
w_PALETA := hllp(w32_hlasky,'_PALETA');
w_MIXBAREV:= hllp(w32_hlasky,'_MIXBAREV');
w_FS_INFO := hllp(w32_hlasky,'_FS_INFO');
w_FS_DISK := hllp(w32_hlasky,'_FS_DISK');
w_ZADAT := hllp(w32_hlasky,'_ZADAT');
w_PRIDEJPOL := hllp(w32_hlasky,'_PRIDEJPOL');
w_UBERPOL := hllp(w32_hlasky,'_UBERPOL');
w_FS_NOVYADRESAR := hllp(w32_hlasky,'_FS_NOVYADRESAR');
w_ZPET := hllp(w32_hlasky,'_ZPET');
w_VPRED := hllp(w32_hlasky,'_VPRED');

w_INFO := hllp(w32_hlasky,'_INFO');
w_DOTAZ := hllp(w32_hlasky,'_DOTAZ');
w_ANONE_DOTAZ := hllp(w32_hlasky,'_ANONE_DOTAZ');
w_CHYBA := hllp(w32_hlasky,'_CHYBA');
w_VLOZ_TEXT := hllp(w32_hlasky,'_VLOZ_TEXT');
w_VYBER_BARVU := hllp(w32_hlasky,'_VYBER_BARVU');
w_FS_ADR_JAKSEMAJMENOVAT := hllp(w32_hlasky,'_FS_ADR_JAKSEMAJMENOVAT');
w_FS_PRO_CTENI := hllp(w32_hlasky,'_FS_PRO_CTENI');
w_FS_SKRYTY := hllp(w32_hlasky,'_FS_SKRYTY');
w_FS_SYSTEMOVY := hllp(w32_hlasky,'_FS_SYSTEMOVY');
w_FS_ARCHIVOVAT := hllp(w32_hlasky,'_FS_ARCHIVOVAT');
w_FS_ZADNE := hllp(w32_hlasky,'_FS_ZADNE');
w_FS_ZPRAVA := hllp(w32_hlasky,'_FS_ZPRAVA');
w_FS_SOUBOR := hllp(w32_hlasky,'_FS_SOUBOR');
w_FS_VELIKOST := hllp(w32_hlasky,'_FS_VELIKOST');
w_FS_B := hllp(w32_hlasky,'_FS_B');
w_FS_KB := hllp(w32_hlasky,'_FS_KB');
w_FS_B2:= hllp(w32_hlasky,'_FS_B2');
w_FS_KB2:= hllp(w32_hlasky,'_FS_KB2');
w_FS_MB2:= hllp(w32_hlasky,'_FS_MB2');
w_FS_GB2:= hllp(w32_hlasky,'_FS_GB2');
w_FS_POSL_ZMENA := hllp(w32_hlasky,'_FS_POSL_ZMENA');
w_FS_ATRIBUTY := hllp(w32_hlasky,'_FS_ATRIBUTY');
w_FS_VYBER_SOUBOR := hllp(w32_hlasky,'_FS_VYBER_SOUBOR');
w_FS_SOUBORNEVYTVOREN := hllp(w32_hlasky,'_FS_SOUBORNEVYTVOREN');
w_FS_ADRESARNEVYTVOREN := hllp(w32_hlasky,'_FS_ADRESARNEVYTVOREN');
w_FS_DISK2 := hllp(w32_hlasky,'_FS_DISK2');
w_FS_DISK3 := hllp(w32_hlasky,'_FS_DISK3');
w_FS_JMENOVKADISKU :=hllp(w32_hlasky,'_FS_JMENOVKADISKU');
w_FS_SYSTEMSOUBORU :=hllp(w32_hlasky,'_FS_SYSTEMSOUBORU');
w_FS_KAPACITA := hllp(w32_hlasky,'_FS_KAPACITA');
w_FS_VOLNEHO :=hllp(w32_hlasky,'_FS_VOLNEHO');
w_FS_SERCISLO := hllp(w32_hlasky,'_FS_SERCISLO');
w_FS_VYBER_NECO := hllp(w32_hlasky,'_FS_VYBER_NECO');
w_FS_JEDNOTKA_NEPRIPRAVENA :=hllp(w32_hlasky,'_FS_JEDNOTKA_NEPRIPRAVENA');
w_PREPSATSOUBOR := hllp(w32_hlasky,'_PREPSATSOUBOR');
w_VSEPOL := hllp(w32_hlasky,'_VSEPOL');
w_VYBPOl := hllp(w32_hlasky,'_VYBPOL');
w_VYBINFO := hllp(w32_hlasky,'_VYBINFO');
w_VYBINFO2 := hllp(w32_hlasky,'_VYBINFO2');


dvojlistbox_vsepol:=w_VSEPOL;
dvojlistbox_vybpol:=w_VYBPOL;
mouse_not_installed_message := hllp(w32_hlasky,'_MOUSE_NOT_INSTALLED');
LFB_not_available_message := hllp(w32_hlasky,'_LFB_NOT_AVAILABLE');
{Dve posledni hlasky pouziva VenomGFX. Ma svoje defaultni, ale timhle se prebijou}
end;


Procedure NactiHlasky_z_venomlink;
var gds:word;
    fcl:pdpole;
    vfcl:^longint;

begin
if global_vnmlink=nil then Exit;    {VenomLink nefunguje?}
gds:=global_vnmlink_ds;
vfcl:=@global_vnmlink[52];
fcl:=pointer(vfcl^);
w_OK := far_hllp(gds,fcl,'_OK');
w_OK2 := far_hllp(gds,fcl,'_OK2');
w_ANO := far_hllp(gds,fcl,'_ANO');
w_NE := far_hllp(gds,fcl,'_NE');
w_CANCEL := far_hllp(gds,fcl,'_CANCEL');
w_CANCEL2 := far_hllp(gds,fcl,'_CANCEL2');
w_POMOC := far_hllp(gds,fcl,'_POMOC');
w_PALETA := far_hllp(gds,fcl,'_PALETA');
w_MIXBAREV:= far_hllp(gds,fcl,'_MIXBAREV');
w_FS_INFO := far_hllp(gds,fcl,'_FS_INFO');
w_FS_DISK := far_hllp(gds,fcl,'_FS_DISK');
w_ZADAT := far_hllp(gds,fcl,'_ZADAT');
w_PRIDEJPOL := far_hllp(gds,fcl,'_PRIDEJPOL');
w_UBERPOL := far_hllp(gds,fcl,'_UBERPOL');
w_FS_NOVYADRESAR := far_hllp(gds,fcl,'_FS_NOVYADRESAR');
w_ZPET := far_hllp(gds,fcl,'_ZPET');
w_VPRED := far_hllp(gds,fcl,'_VPRED');

w_INFO := far_hllp(gds,fcl,'_INFO');
w_DOTAZ := far_hllp(gds,fcl,'_DOTAZ');
w_ANONE_DOTAZ := far_hllp(gds,fcl,'_ANONE_DOTAZ');
w_CHYBA := far_hllp(gds,fcl,'_CHYBA');
w_VLOZ_TEXT := far_hllp(gds,fcl,'_VLOZ_TEXT');
w_VYBER_BARVU := far_hllp(gds,fcl,'_VYBER_BARVU');
w_FS_ADR_JAKSEMAJMENOVAT := far_hllp(gds,fcl,'_FS_ADR_JAKSEMAJMENOVAT');
w_FS_PRO_CTENI := far_hllp(gds,fcl,'_FS_PRO_CTENI');
w_FS_SKRYTY := far_hllp(gds,fcl,'_FS_SKRYTY');
w_FS_SYSTEMOVY := far_hllp(gds,fcl,'_FS_SYSTEMOVY');
w_FS_ARCHIVOVAT := far_hllp(gds,fcl,'_FS_ARCHIVOVAT');
w_FS_ZADNE := far_hllp(gds,fcl,'_FS_ZADNE');
w_FS_ZPRAVA := far_hllp(gds,fcl,'_FS_ZPRAVA');
w_FS_SOUBOR := far_hllp(gds,fcl,'_FS_SOUBOR');
w_FS_VELIKOST := far_hllp(gds,fcl,'_FS_VELIKOST');
w_FS_B := far_hllp(gds,fcl,'_FS_B');
w_FS_KB := far_hllp(gds,fcl,'_FS_KB');
w_FS_B2:= far_hllp(gds,fcl,'_FS_B2');
w_FS_KB2:= far_hllp(gds,fcl,'_FS_KB2');
w_FS_MB2:= far_hllp(gds,fcl,'_FS_MB2');
w_FS_GB2:= far_hllp(gds,fcl,'_FS_GB2');
w_FS_POSL_ZMENA := far_hllp(gds,fcl,'_FS_POSL_ZMENA');
w_FS_ATRIBUTY := far_hllp(gds,fcl,'_FS_ATRIBUTY');
w_FS_VYBER_SOUBOR := far_hllp(gds,fcl,'_FS_VYBER_SOUBOR');
w_FS_SOUBORNEVYTVOREN := far_hllp(gds,fcl,'_FS_SOUBORNEVYTVOREN');
w_FS_ADRESARNEVYTVOREN := far_hllp(gds,fcl,'_FS_ADRESARNEVYTVOREN');
w_FS_DISK2 := far_hllp(gds,fcl,'_FS_DISK2');
w_FS_DISK3 := far_hllp(gds,fcl,'_FS_DISK3');
w_FS_JMENOVKADISKU :=far_hllp(gds,fcl,'_FS_JMENOVKADISKU');
w_FS_SYSTEMSOUBORU :=far_hllp(gds,fcl,'_FS_SYSTEMSOUBORU');
w_FS_KAPACITA := far_hllp(gds,fcl,'_FS_KAPACITA');
w_FS_VOLNEHO :=far_hllp(gds,fcl,'_FS_VOLNEHO');
w_FS_SERCISLO := far_hllp(gds,fcl,'_FS_SERCISLO');
w_FS_VYBER_NECO := far_hllp(gds,fcl,'_FS_VYBER_NECO');
w_FS_JEDNOTKA_NEPRIPRAVENA :=far_hllp(gds,fcl,'_FS_JEDNOTKA_NEPRIPRAVENA');
w_PREPSATSOUBOR := far_hllp(gds,fcl,'_PREPSATSOUBOR');
w_VSEPOL := far_hllp(gds,fcl,'_VSEPOL');
w_VYBPOl := far_hllp(gds,fcl,'_VYBPOL');
w_VYBINFO := far_hllp(gds,fcl,'_VYBINFO');
w_VYBINFO2 := far_hllp(gds,fcl,'_VYBINFO2');


dvojlistbox_vsepol:=w_VSEPOL;
dvojlistbox_vybpol:=w_VYBPOL;
mouse_not_installed_message := far_hllp(gds,fcl,'_MOUSE_NOT_INSTALLED');
LFB_not_available_message := far_hllp(gds,fcl,'_LFB_NOT_AVAILABLE');
{Dve posledni hlasky pouziva VenomGFX. Ma svoje defaultni, ale timhle se prebijou}
end;


Procedure NactiHlasky_ze_souboru_nebo_z_venomlink(s:string);
{Pokud bezi VenomLink, nacte z nej, pokud ne, tak ze souboru}
begin
if global_vnmlink=nil
   then NactiHlasky_ze_souboru(s)
   else NactiHlasky_z_VenomLink;
end;