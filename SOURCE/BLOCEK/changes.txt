BLOČEK VERSION HISTORY


1.75
    * tries to be more compatible with virtual environments
    - copying texts from GUI dialogs to edit windows works better now
    + if you try to copy text to clipboard without any text selected, the word on cursor position will be used
    - after every 65536 bytes loader added invalid character to loaded text
    * slightly improved color selection dialog    
    + functions for line expanding
    + optional spaces emphasing
    + Find dialog offers the phrase occurrences counting
    + right mouse click offers new word related functions     
    + right mouse click (or Ctrl-L) offers new line related functions 
    + correctly replaces Unicode characters above 0xFFFF to character 0xFFFD "�"
    + keyboard shortcuts in the main menu
    * main menu does not allow to select functions which aren't currently applicable
    * buttons in some situations accept keyboard shortcuts without pressed ALT
    * unicode definition tables are now separated to own TBL subdirectory

1.74
    + bookmarks (accessible by right mouse button click or from main menu)
    - deleting a text block sometimes crashed the program
    - key "delete" sometimes incorrectly changed the scope of the text block
    - the non-resident part of the keyboard handler is more compatible with WinXP
    - also the resident part of the keyboard handler is improved (see TECHNOTE.TXT)
    - mouse wheel works again (with DOS driver CTMouse or with DOSBOX-X)
    + optimalizations in the program messages management
    * the hotkey for text block delete changed from CTRL-Space to Shift-Del
       (to be consistent with one line input dialogs) 
    - fixed several bugs in command line processing
    - other small bugfixes

1.73
    + cooperates with Windows clipboard
    * mouse driver improvements
    + functions for choosing related chars (via CTRL-Q or mouse right click submenu)
    + distribution package with integrated DosBox-X
    + speed optimizations in editor: if possible, only the updated part of window will be rendered
    + system info window shows whether the OS allows to use SSE instructions
    + optional editor window left side panel showing line ending type
    + text selection range is properly updated during text operations 
    - various small bugfixes

1.72
    + french and turkish translations
    - fixed scrolling of the line with cards of opened files
    - fixed possible crash after closing a file
    * minor improvements in the system information window
    + the GUI system can now process a multiline buttons 
      (currently used in the russian interface in fileselector)
    - fixed some conflicts between BIOS/DOS keyboard drivers and the internal driver
      (details described in the TECHNOTE.TXT file)

1.71
    + german translation
    + fast switching among opened files with Ctrl+Plus and Ctrl+Minus
    - fixed some bugs (including crashes)
    * changed the picture for welcome screen

1.7
    + many improvements in the graphical user interface (GUI)
    + most of the windows can be moved by dragging their title bar by mouse
    + slovak keyboard layout
    + blocks of text can be selected by dragging the mouse with pressed left button
    + blocks of text can be selected not only in the edited text but also in most places of the GUI like information or help windows.
    + the scope of the Search or Search and Replace functions can be limited to text block only
    + many powerful conversion routines handling the code pages and Unicode
    * if the document is not in Unicode it is always in some defined DOS code page
    - The RAM size detection was wrong on some systems
    + Now can work even with graphic cards with non-continuous video memory (like GeForce RTX 2060)
    + can load and save with all three line-ending standards (CRLF, CR, LF)
    + can load and save Unicode files encoded in UTF-16LE and UTF-16BE
    + recognizes the Unicode BOMs (byte order marks)
    + customizable word wrapping
    + now can fixate the word-wrapping not only for the whole document but also for selected block only.
    + function for intelligent merging the legacy DOS style lines (78 or 80 chars) to word-wrapping blocks.  
    - better keyboard handler 
    - fixed bug in the conversion routine for 3-bytes long Unicode glyphs
    + conversion from HTML entities into Unicode chars
    + QR codes creation
    - Program can't be shot down by Ctrl-Break
    - fixed tabulators
    - corrected minor bugs in the color selection dialog
    - functions for moving to next word or to previous word now scroll the window if needed
    + the single-line text boxes can use the clipboards too (like f.e. in „Search and replace“ dialog)
    + more informations in the system information window 
    * the number of clipboards reduced to 7 (still more than enough) leaving more space on the upper line for menu
    x the language files for german, turkish, hungarian and french languages are not updated so they are moved from "public" TRANSLAT subdirectory to TRANSLAT\VER162 - they can partialy work after copying back into TRANSLAT

1.62
    - fixed a lot of typos in texts
    - fixed block operations on the single line blocks
    - solved "undeletable" first char of line which occurred in some situations
    + block conversions to make case up or low

1.61
    + Conversion of the extended ASCII or Unicode chars into HTML entities
    - fixed possible crash when opening large text file
    - fixed crash after pressing up arrow after menu activation by mouse
    * progress indicator now uses timer
     
1.6
    - fixed graphical glitch causing garbage in some circumstances when opening a new window
    - mouse sometimes caused noticeable slowdown of program
    - reenabled  the char typing using ALT + <codes on numeric keyboard>
    - fixed crash when "word wrapping" on empty file was selected
    - optimized the method for getting the hardware glyphs from graphic card
    + implemented function keys CTRL+right arrow and CTRL+left arrow
    + implemented function CTRL+A (select all)
    + CTRL+Del deletes selected block
    + selected blocks are now visible just in the text - not only in status line     
    + program language can be switched directly from menu system (no manual editing of conf. files is needed)     
    * not only the final compilation but also regular development is done with Freepascal 3.0.2

1.5c  
    + now can display the Exif info in JPEG files and export the Exif data
    + can resize big images to fit the screen optimally
    + can now properly display the semi-standard VGA fonts with used 7.bit in char definitions 
    - fixed few bugs which potentially could lead to crash
    - fixed some graph. glitches when ch*anging the image viewer and text editor modes
    - fixed "locking" of ALT and CTRL keys in some situations
    - fixed typos in english messages

1.4b 
    - fixed potential bug with keyboard handler and corrected system info window

1.4
    * detect DOS code page for using with Unicode
      (so "keyboard_mapping" variable in BLOCEK.CFG is now deprecated)
    * some VESA BIOSes or drivers are buggy so VESA3 functions can be disabled now
    + block operations - you have not only one clipboard but NINE clipboards!
    + System information window

1.37
    - Fixed conflict between DOS national keyboard driver and Bloček's internal
      keyboard driver. ("dead keys" watched by DOS was "stealed" and not processed
      by internal driver)
    - Fixed wrong displaying of text cursor in some situations. (this bug became more
      severe in previous version)
    + Now allows to define keys which are "semi-capslock dependent"
      (like ěĚ/2 in czech keyboard driver)
    * keyboard definition files are now much more user friendly so it is easier to
      define new ones
    + hungarian keyboard layout

1.36b
    - Fixed some annoying problems with charmap
    * Avoids more Free Pascal 2.x.x bugs carefully - it is now more stable

1.35b
    * Now can work in banked modes too!
    - Prior versions with some screen drivers could download wrong fonts from VRAM.

1.34b
    - fixed some minor problems with line breaking in GUI
    - fixed wrong positions of points in radio button dialogs

1.34
    + Now can load vector image format .WMF
    - fixed occasional bug when shelling into DOS
    * compiled with current version of graphics library VenomGFX

1.33b
    + ASCII conversions
    + brasil keyboard layout
    + configurable menus
    + warns if virtual memory is not present
    * radically reworked all management of messages and texts
    * other invisible changes
    * for final compilation is used Freepascal 2.2.0a
    - crash when checking word wrapping with new file
    - filesector allows to manually select a file by writing its name
    - tabs work correctly now
    - thousands separator

1.32c
    + charmap
    + configuration management with default and user configuration files
    - few bugs fixed
    * files are opened gradually and loading can be canceled
    * quite a lot of code rewritten
    * better speed
    * for final compilation is used another compiler - Freepascal 2.1.4
    + more Unicode fonts (there is also a editor with TTF import available on my WWW)

1.31
    * reworked code for switching of opened files
    * better tabulators handling
    - more compatible searching files
    + russian keyboard driver knows letters "yo" and "YO"
    * all text windows now share one output buffer - saves a lot of memory
    - crash in Search again after Search without any occurrence of searched string
    * more careful checks if graphics card supports LFB

1.3c
    - functions Search, Search again, Replace and Fix word wrapping
    + better algorithms for scaling images (see Select filter in Options menu)
    + horizontal and vertical flip for images
    * scrolbars work more or less like in windows
    + changing to/from word wrapping mode keeps cursor position
    + checks if image files are corrupted
    - fixed some bugs in image viewing mode
    + added german keyboard driver
    - fixed czech keyboard driver
    * many invisible but important changes in GUI library
    + better error handler
    + expands tabs

1.2d
    + PNG reader
    + Ctrl+Alt+arrow functions
    - hopefuly fixed one text scrolling bug with word wrapping
    + Bloček can be now used as image viewer (BMP, PCX, GIF, PNG, JPG)
    - reports error when writing to read-only drive
    - now can't become 15bpp x 16bpp graphics mode confusion

1.1.2
    + another Unicode font
    * Unicode fonts are much smaller now
    * Unicode fonts are now proportional
    + keyboard layout switching
    + GIF reader
    - some small bugs fixed (some left)
    + glyph info on right mouse button

1.1
    + word wrapping
    + conversion routines to/from Unicode
    - corrected some small bugs
    + automatic detection of Unicode text
    * some internal changes what will be visible later :-)
    ! most probably some new bugs around word wrapping

1.0.x
    gradual evolution