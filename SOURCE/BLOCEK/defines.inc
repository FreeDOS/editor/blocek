{Sem zapis vsechny definovane symboly}
{$I-}   {Implicitni je totiz $I+ a tato direktiva je lokalni, nikoliv globalni}
        {Po IO operacich bych mel tudiz vzdy volat IOresult}
{$H-}
{$IFNDEF FPC}Tyto jednotky se daji prelozit jedine ve Freepascalu{$ENDIF}
{$IFNDEF GO32V2}{$FATAL Tyto jednotky lze prelozit jedine pro system DOS (Go32V2)}{$ENDIF}
{$MODE FPC}
{$IFDEF VER2}{$DEFINE NEWFPC}{$ENDIF}
{$IFDEF VER3}{$DEFINE NEWFPC}{$ENDIF}
{$IFDEF NEWFPC}{$CALLING OLDFPCCALL}{$ENDIF}

{$IFDEF FPC_TP}{$WARNING V IDE bys mel zrusit mod kompatibility s TP}{$ENDIF}
{$IFDEF RELEASE}
{$DEFINE JPG_SUPPORT}
{$NOTE Extend unit directory variable to .\PASJPEG\}
{$ENDIF}
{$IFDEF ZakazDebug}
{$Q-}
{$R-}
{$S-}
{$D-}
{$ENDIF}
{$IFDEF DEBUG}
(*{$CHECKPOINTER ON}*)
{$ENDIF}
{_$DEFINE JPG_SUPPORT}         {Pokud chces podpotu obrazku JPG, odmazni}
