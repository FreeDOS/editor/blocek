uses venomgfx;


Procedure ArrayToBWSprite(ar:pointer;ar_width,ar_height:longint;var sprite:virtualwindow;vb,c1,c2:word);
{AR: ukazatel na pole bajtu - nulove hodnoty budou interpretovany jako
 dlazdice s barvou C1, nenulove s barvou C2.

 AR_width: sirka v dvojrozmerne interpretaci pole
 AR_height: vyska zdrojoveho pole

 SPRITE - bude vytvoren
 Vb - velikost strany ctverecku, ktery se vytvori z kazdeho bodu vstupniho pole}

var x,y,jj,s,v,n,m:longint;
    ap:pchar;
    w:word;
    l:^word;
    d:byte;

begin
if (ar=nil) or (ar_width<=0) or (ar_height<=0) or (vb<=0) then
   begin
   {VynulovaniPolozek_VWwindow(sprite);}
   Exit;
   end;
Init_VW(sprite,ar_width*vb,ar_height*vb,false);
ap:=ar;
l:=pointer(sprite.vwoffset);
for y:=0 to ar_height-1 do
    for jj:=1 to vb do
        begin
        for x:=0 to ar_width-1 do
            begin
            n:=ar_width*y+x;
            d:=byte(ap[n]);
            if d=0 then w:=c2 else w:=c1;

            for m:=1 to vb do
                begin
                {PutPixel(sprite,x,y,w);}
                if w=0 then write('*') else write('.');

                l^:=w;
                inc(l);

                end;
            end;
        writeln;
        end;
end;


const ar_si = 5;
      ar_vy = 6;

var
    p:pchar;
    s1,s2:string;
    ar:packed array[0..AR_si-1,0..ar_vy-1] of byte;
    an:array[1..ar_si*ar_vy] of byte;

    i,j,a:longint;
    v:virtualwindow;

begin

init_graph(find_mode(320,200));


Load_BMP('.\sys_bmp\lupa.bmp',v);

Save_Sprite_to_INC_file(v,'lupa.inc');

a:=0;
for j:=0 to ar_vy-1 do
    for i:=0 to ar_si-1 do
        begin
        ar[i,j]:=byte(odd(a));
        inc(a);
        end;

for j:=1 to ar_si*ar_vy do an[j]:=byte(odd(j));


ArrayToBWSprite(@an,ar_si,ar_vy,v,3,64000,0);

bar(vga,100,100,250,200,1234);
PutSprite(vga,v,102,102);
{Line(vga,10,11,30,11,60);
Line(vga,11,12,11,12+40-1,600);}
readln;

kill_graph;

end.
