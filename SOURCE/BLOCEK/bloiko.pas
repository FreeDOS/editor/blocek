{Cast zdrojoveho kodu pro program Blocek}

unit bloiko;
interface

Function ZalozkaNorm(b:byte):pointer;  {ve skutecnosti PVirtualWindow}
Function ZalozkaPlus(b:byte):pointer;  {ve skutecnosti PVirtualWindow}
Function MyIkonkaBryle:pointer;        {ve skutecnosti PVirtualWindow}



implementation
uses BloIkoD,VenomGFX;


var internal_zalozka_cislo:array[1..5] of virtualwindow;
    internal_zalozka_cislo_plus:array[1..4] of virtualwindow;
    internal_ikonka_bryle:virtualwindow;



Function MyIkonkaBryle:pointer;
begin
MyIkonkaBryle:=@internal_ikonka_bryle;
end;


Function ZalozkaNorm(b:byte):pointer;  {ve skutecnosti PVirtualWindow}
begin
if b<1 then b:=1;
if b>5 then b:=5;
ZalozkaNorm:=@internal_zalozka_cislo[b];
end;


Function ZalozkaPlus(b:byte):pointer;  {ve skutecnosti PVirtualWindow}
begin
if b<1 then b:=1;
if b>4 then b:=4;
ZalozkaPlus:=@internal_zalozka_cislo_plus[b];
end;


Procedure InicializujJednotku;
var a:byte;
begin

Load_Sprite_from_INC_data(internal_zalozka_cislo[1],
                          @ZALOZKA_1_DATA,
                          ZALOZKA_1_WIDTH,
                          ZALOZKA_1_HEIGHT);
Load_Sprite_from_INC_data(internal_zalozka_cislo[2],
                          @ZALOZKA_2_DATA,
                          ZALOZKA_2_WIDTH,
                          ZALOZKA_2_HEIGHT);
Load_Sprite_from_INC_data(internal_zalozka_cislo[3],
                          @ZALOZKA_3_DATA,
                          ZALOZKA_3_WIDTH,
                          ZALOZKA_3_HEIGHT);
Load_Sprite_from_INC_data(internal_zalozka_cislo[4],
                          @ZALOZKA_4_DATA,
                          ZALOZKA_4_WIDTH,
                          ZALOZKA_4_HEIGHT);
Load_Sprite_from_INC_data(internal_zalozka_cislo[5],
                          @ZALOZKA_5_DATA,
                          ZALOZKA_5_WIDTH,
                          ZALOZKA_5_HEIGHT);

Load_Sprite_from_INC_data(internal_zalozka_cislo_plus[1],
                          @ZALOZKA_1P_DATA,
                          ZALOZKA_1P_WIDTH,
                          ZALOZKA_1P_HEIGHT);
Load_Sprite_from_INC_data(internal_zalozka_cislo_plus[2],
                          @ZALOZKA_2P_DATA,
                          ZALOZKA_2P_WIDTH,
                          ZALOZKA_2P_HEIGHT);
Load_Sprite_from_INC_data(internal_zalozka_cislo_plus[3],
                          @ZALOZKA_3P_DATA,
                          ZALOZKA_3P_WIDTH,
                          ZALOZKA_3P_HEIGHT);
Load_Sprite_from_INC_data(internal_zalozka_cislo_plus[4],
                          @ZALOZKA_4P_DATA,
                          ZALOZKA_4P_WIDTH,
                          ZALOZKA_4P_HEIGHT);

Load_Sprite_from_INC_data(internal_ikonka_bryle,
                          @IKONKA_BRYLE,
                          IKONKA_BRYLE_WIDTH,
                          IKONKA_BRYLE_HEIGHT);
end;



begin
InicializujJednotku;
end.
