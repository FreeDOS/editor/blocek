{SCANKODY, ktere prichazeji pres nas handler. Pozor, handler zachycuje port
 60h, zatimco DOS a jeho ovladace vetsinou 64h. Tento port pro nektere
 klavesy vraci kody jine. Proto rada techto konstant nelze pouzit pro
 scankody zjistovane DOSem, pres preruseni 16h}

const
      Priznak_sedych_sipek = 30;
      Priznak_jednoty      = 57;

      SCANKEY_UNKNOWN = 127; {pro nezname kody}
      SCANKEY_ESC = 1;
      SCANKEY_F1 = 59;
      SCANKEY_F2 = 60;
      SCANKEY_F3 = 61;
      SCANKEY_F4 = 62;
      SCANKEY_F5 = 63;
      SCANKEY_F6 = 64;
      SCANKEY_F7 = 65;
      SCANKEY_F8 = 66;
      SCANKEY_F9 = 67;
      SCANKEY_F10 = 68;
      SCANKEY_F11 = 87;
      SCANKEY_F12 = 88;
      SCANKEY_LITTLE_BALL = 41;
      SCANKEY_1 = 2;
      SCANKEY_2 = 3;
      SCANKEY_3 = 4;
      SCANKEY_4 = 5;
      SCANKEY_5 = 6;
      SCANKEY_6 = 7;
      SCANKEY_7 = 8;
      SCANKEY_8 = 9;
      SCANKEY_9 = 10;
      SCANKEY_0 = 11;
      SCANKEY_DASH = 12;
      SCANKEY_EQUALSIGN = 13;
      SCANKEY_BACKSLASH = 14;
      SCANKEY_TAB = 15;
      SCANKEY_Q = 16;
      SCANKEY_W = 17;
      SCANKEY_E = 18;
      SCANKEY_R = 19;
      SCANKEY_T = 20;
      SCANKEY_Z = 21;
      SCANKEY_U = 22;
      SCANKEY_I = 23;
      SCANKEY_O = 24;
      SCANKEY_P = 25;
      SCANKEY_LSB = 26;   {Left square bracket}
      SCANKEY_ESB = 27;   {Right square bracket}
      SCANKEY_G_ENTER = 28;
      SCANKEY_ENTER = 84+Priznak_jednoty;
      SCANKEY_LCTRL = 29;
      SCANKEY_PCTRL = 89+30;
      SCANKEY_CTRL  = 89+Priznak_jednoty;
      SCANKEY_LALT = 56;
      SCANKEY_PALT = 91+30;
      SCANKEY_ALT  = 91+Priznak_jednoty;
      SCANKEY_SHIFT = 90+Priznak_jednoty;
      SCANKEY_SPACE = 57;
      SCANKEY_CAPS_LOCK = 58;
      SCANKEY_LSHIFT = 42;
      SCANKEY_A = 30;
      SCANKEY_S = 31;
      SCANKEY_D = 32;
      SCANKEY_F = 33;
      SCANKEY_G = 34;
      SCANKEY_H = 35;
      SCANKEY_J = 36;
      SCANKEY_K = 37;
      SCANKEY_L = 38;
      SCANKEY_OE = 39;
      SCANKEY_AE = 40;
      SCANKEY_CROSS = 43;
      SCANKEY_LOWER_THAN = 86;
      SCANKEY_Y = 44;
      SCANKEY_X = 45;
      SCANKEY_C = 46;
      SCANKEY_V = 47;
      SCANKEY_B = 48;
      SCANKEY_N = 49;
      SCANKEY_M = 50;
      SCANKEY_SEMICOLON = 51;
      SCANKEY_DOUBLEDOT = 52;
      SCANKEY_MINUS = 53;
      SCANKEY_PSHIFT = 54;
      SCANKEY_WINDOWS_CONTEXT = 93;
      SCANKEY_SCROLL_LOCK = 70;
      SCANKEY_INSERT = 82+Priznak_jednoty;
      SCANKEY_REMOVE = 83+Priznak_jednoty;
      SCANKEY_POS1 = 71+Priznak_jednoty;
      SCANKEY_END = 79+Priznak_jednoty;
      SCANKEY_PGUP = 73+Priznak_jednoty;
      SCANKEY_PGDOWN = 81+Priznak_jednoty;
      SCANKEY_CURSOR_LEFT = 75+Priznak_jednoty;
      SCANKEY_CURSOR_RIGHT = 77+Priznak_jednoty;
      SCANKEY_CURSOR_UP = 72+Priznak_jednoty;
      SCANKEY_CURSOR_DOWN = 80+Priznak_jednoty;
      SCANKEY_NUM = 69;
      SCANKEY_NUM_DIVIDE = 53;
      SCANKEY_NUM_MUL = 55;
      SCANKEY_NUM_MINUS = 74;
      SCANKEY_NUM_PLUS = 78;
      SCANKEY_NUM_1 = 79;
      SCANKEY_NUM_2 = 80;
      SCANKEY_NUM_3 = 81;
      SCANKEY_NUM_4 = 75;
      SCANKEY_NUM_5 = 76;
      SCANKEY_NUM_6 = 77;
      SCANKEY_NUM_7 = 71;
      SCANKEY_NUM_8 = 72;
      SCANKEY_NUM_9 = 73;
      SCANKEY_NUM_0 = 82;
      SCANKEY_NUM_COMMA = 83;
      SCANKEY_NUM_ENTER = 84+Priznak_sedych_sipek;

      SCANKEY_G_INSERT = 82+Priznak_sedych_sipek;
      SCANKEY_G_REMOVE = 83+Priznak_sedych_sipek;
      SCANKEY_G_POS1 = 71+Priznak_sedych_sipek;
      SCANKEY_G_END = 79+Priznak_sedych_sipek;
      SCANKEY_G_PGUP = 73+Priznak_sedych_sipek;
      SCANKEY_G_PGDOWN = 81+Priznak_sedych_sipek;
      SCANKEY_G_CURSOR_LEFT = 75+Priznak_sedych_sipek;
      SCANKEY_G_CURSOR_RIGHT = 77+Priznak_sedych_sipek;
      SCANKEY_G_CURSOR_UP = 72+Priznak_sedych_sipek;
      SCANKEY_G_CURSOR_DOWN = 80+Priznak_sedych_sipek;

      SCANKEY_WINDOWS_LEFT = 91;
      SCANKEY_WINDOWS_RIGHT = 92;
      SCANKEY_WINDOWS_MENU = 93;
      SCANKEY_PRINT = 149;
      SCANKEY_PAUSE = 150;
      SCANKEY_CTRLBREAK = 151;


ScanKey_to_eng_ascii : array['a'..'z'] of byte = (30,48,46,32,18,33,34,35{h},
                                                  23,36,37,38,50,49,24,25{p},
                                                  16,19,31,20,22,47,17,45{x},
                                                  21{y},44 {z});

