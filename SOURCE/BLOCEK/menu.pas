p:=StromDef(
Vetev(VytvorPolozku('Soubor','',1,TRUE),
   UzelS(VytvorPolozku('Novy','',1,TRUE),
   UzelS(VytvorPolozku('Otevrit','',1,TRUE),
   UzelS(VytvorPolozku('Ulozit','',1,TRUE),
   UzelS(VytvorPolozku('Zavrit','',1,TRUE),
   UzelS(VytvorPolozku('Prikazovy radek','',1,TRUE),
   UzelS(VytvorPolozku('Konec','',1,TRUE),nil)))))),
SvazejS(
Vetev(VytvorPolozku('Text','',1,TRUE),
   UzelS(VytvorPolozku('ASCII prevody','',1,TRUE),
   Vetev(VytvorPolozku('Blokove operace','',1,TRUE),
      UzelS(VytvorPolozku('znaky -> HTML entity','',1,TRUE),
      UzelS(VytvorPolozku('HTML entity -> znaky','',1,TRUE),
      UzelS(VytvorPolozku('Na velka','',1,TRUE),
      UzelS(VytvorPolozku('Na mala','',1,TRUE),nil)))),
   SvazejS(
   Vetev(VytvorPolozku('Zalamovani textu','',1,TRUE),
      UzelS(VytvorPolozku('Zafixuj Nalamani','',1,TRUE),
      UzelS(VytvorPolozku('Spojovani radek','',1,TRUE),nil)),
   SvazejS(
   UzelS(VytvorPolozku('Hledej','',1,TRUE),
   UzelS(VytvorPolozku('Hledej zase','',1,TRUE),
   UzelS(VytvorPolozku('Zamena','',1,TRUE),
   UzelS(VytvorPolozku('Mapa znaku','',1,TRUE),
   UzelS(VytvorPolozku('QR kod','',1,TRUE),nil))))))
   )))),
SvazejS(
Vetev(VytvorPolozku('Nastaveni','',1,TRUE),
   Vetev(VytvorPolozku('Nastaveni barev','',1,TRUE),
      UzelS(VytvorPolozku('popredi','',1,TRUE),
      UzelS(VytvorPolozku('pozadi','',1,TRUE),
      UzelS(VytvorPolozku('desktop','',1,TRUE),
      UzelS(VytvorPolozku('prosvetlene','',1,TRUE),nil)))),
   SvazejS(
   UzelS(VytvorPolozku('Zmena jazyka','',1,TRUE),
   UzelS(VytvorPolozku('Prosvecuj radku','',1,TRUE),
   UzelS(VytvorPolozku('Zobrazuj konce radek','',1,TRUE),
   UzelS(VytvorPolozku('Zmena fontu','',1,TRUE),
   UzelS(VytvorPolozku('Obrazovy filtr','',1,TRUE),
   UzelS(VytvorPolozku('preferovane klavesnice','',1,TRUE),
   UzelS(VytvorPolozku('Ulozit nastaveni','',1,TRUE),nil)))))))
   )),
SvazejS(
Vetev(VytvorPolozku('Napoveda','',1,TRUE),
   UzelS(VytvorPolozku('O programu','',1,TRUE),
   UzelS(VytvorPolozku('Nacist napovedu INFO.TXT','',1,TRUE),
   UzelS(VytvorPolozku('Systemove informace','',1,TRUE),nil))),
nil
)))))))
);