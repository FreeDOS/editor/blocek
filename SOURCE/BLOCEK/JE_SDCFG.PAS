{Detekce, je-li zasunuta SD karta ve ctecce SINTECHI}
uses ide;
{$I-}

Procedure Nacti_IDE_info(b:byte);
begin
case b of
  1:IDE_ATAPI[1].Nastav($1F0,true);
  2:IDE_ATAPI[2].Nastav($1F0,false);
  3:IDE_ATAPI[3].Nastav($170,true);
  4:IDE_ATAPI[4].Nastav($170,false);
end;{case}
end;


Function Je_Sintechi(s:string):boolean;
var b:byte;
begin
b:=Pos('SINTECHI',s);
Je_Sintechi:=b<>0;
end;


Procedure CopyFile(s,t:string);
const velbuf = 2048;
var f,g:file;
    c:byte;
    w:word;
    buf:array[0..velbuf-1] of byte;
begin
assign(f,s);
assign(g,t);
rewrite(g,1);
reset(f,1);
if IOresult<>0 then
   begin     {nenalezen soubor S ?}
   c:=$c3;   {operacni kod instrukce RET}
   blockwrite(g,c,1);
   close(g);
   exit;
   end
   else begin
   repeat
   BlockRead(f,buf,velbuf,w);
   BlockWrite(g,buf,w);
   until w<>velbuf;
   close(g);
   close(f);
   end;
end;


Function Nacti_Celou_IDE_Konfiguraci:byte;
var a:byte;
    s:string;
begin
Nacti_ide_atapi_konfiguraci;
for a:=1 to 4 do
    begin
    s:=IDE_ATAPI[a].Vrat_Oznaceni_Modelu;
    if Je_sintechi(s) then begin Nacti_Celou_IDE_Konfiguraci:=a;Exit;end;
    end;
Nacti_Celou_IDE_Konfiguraci:=0;
end;


var a,b:byte;
    s,t:string;
    i:integer;
    f:text;

    m:file;

begin
assign(f,'');
rewrite(f);
if Paramcount>1 then b:=Nacti_Celou_IDE_Konfiguraci
   else begin
   if Paramcount=0 then t:='3' else t:=ParamStr(1);
   Val(t,a,i);
   if i<>0 then b:=Nacti_Celou_IDE_Konfiguraci
      else begin
      b:=a;
      Nacti_IDE_Info(b);
      s:=IDE_ATAPI[b].Vrat_Oznaceni_Modelu;
      if Je_Sintechi(s) then b:=a else b:=0;
      end;
   end;

if b=0 then writeln(f,'SD ctecka SINTECHI nenalezena')
       else writeln(f,'SD ctecka nalezena na pozici c.',b);

if b=0 then begin close(f);Halt(0);end;

{Assign(m,'sd.dat');
rewrite(m,1);
blockwrite(m,IDE_ATAPI[b].dev_info^,512);
close(m);}

if IDE_ATAPI[b].dev_info^[1]=65 then
   begin
   writeln(f,'Ve ctecce jsem SD kartu nenasel.');
   close(f);
   CopyFile('Dummy.com','drvexch2.com');

   Halt(0)
   end
   else begin
   s:=IDE_ATAPI[b].Vrat_Seriove_Cislo;
   writeln(f,'Ve ctecce jsem nasel SD kartu se seriovym cislem ',s);
   close(f);
   CopyFile('drvexch.com','drvexch2.com');
   Halt(100);
   end;
end.