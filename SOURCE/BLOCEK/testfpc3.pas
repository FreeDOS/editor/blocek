{$CALLING OLDFPCCALL}

var global_x, global_y:longint;

Procedure clipping;assembler;{$IFDEF FPC3}nostackframe;{$ENDIF}
{INPUT:
 in EAX expects X coordinate
 in EBX expects Y coordinate

 OUTPUT:
 in EAX modified X coord.
 in EBX modifies Y coord.
 }
asm
add eax,100
add ebx,100
end;


procedure dummy_draw(x,y:longint);assembler;
asm
mov eax,x
mov ebx,y
call clipping {??? Why now in EBX is not the new value but the old one ???}

mov global_x,eax
mov global_y,ebx
end;

{----main----}
begin
dummy_draw(1,2);

writeln('X: ',global_x); {prints "101" which is correct}
writeln('Y: ',global_y); {prints "2" but should print "102" instead}

end.
