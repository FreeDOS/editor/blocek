

uses Go32;
var r:TRealRegs;
begin


FillChar(r,sizeof(TRealRegs),0);

r.cs:=tb_segment;
r.ip:=tb_offset;
mem[tb_segment:tb_offset]:=$cb;  {opcode for RETF}

asm
mov eax,0301h
xor ecx, ecx
xor ebx,ebx
lea edi,r
int 31h         {Function INT31h/301h (CallRealModeFarRoutine)}
end;

writeln('OK');

end.
