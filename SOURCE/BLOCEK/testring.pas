program TestRing;
uses crt;

var p:pchar;
    a:ansistring;


begin
{t.Init(15);}      {buffer 15 bajtu}

p:='Font files in <barva=65535>TTF<sb> format can''t be imported directly.'+#13#10+
   'You have to use a external conversion tool <barva=51200>TTF2PCX<sb>'+#13#10+
   'This tool you can find in <barva=65535>.\TTF2PCX<sb> subdirectory.'+#13#10+
   'It is a windows application which converts TTF font into PCX image.'+#13#10+
   'Usage:'+#13#10+
   'In dialog windows select font, variant and size'+#13#10+
   'Ensure that options are set in this way: (it is important)'+#13#10+
   '<barva=65535>min. char:<sb> 0x20'+#13#10+
   '<barva=65535>max. char:<sb> 0xFFFF'+#13#10+
   '<barva=65535>output: <sb>monochrome'+#13#10+
   'After this step you have to convert the created PCX'+#13#10+
   'file into <barva=65535>BMP<sb> file (with any image converter)'+#13#10+
   'Such BMP copy into same directory where <barva=51200>Ka�m�r<sb> resides'+#13#10+
   '<font=cour14.fn>Unfortunately TTF2PCX has a bug. It claims it can convert<sb>'+#13#10+
   'chars up to 65535, but in the fact handles chars only up to 32767<sf>';

writeln(p);
writeln(length(p));

a:=p;
writeln(a);
writeln(length(a));

end.
