Between 1.74 and 1.75 =======================================================
What has been changed and what should be translated to DE, FR, TR


Completely new
#menu_slovo
##dialogy_menu_slovo
##menu_radek
##dialogy_menu_radek1
##dialogy_menu_radek2
##dialogy_menu_radek3
#doprad
#doprad_txt
#zarovnani


Changed:
#m29
#pocet_zmen

Changed - not necessarily text but added the keyboard shortcuts
- all menu labels (m<number>, like m2, m45,...)
#vyhled_mozn
#roz_vel_pismen
#potvrzovat
#nahradvse
#jen_v_bloku

Deleted:
#vice_info_o_znaku

-----------------------------------------------------------------------------

Between 1.73 and 1.74 =======================================================
What has been changed and what should be translated to DE, FR, TR

Completely new (for correction, used DeepL):

#zalozky
#m460


Changed:
#napoveda_mys (added last sentence mentioning bookmarks)

-----------------------------------------------------------------------------

Between 1.72 and 1.73 =======================================================
What has been changed and what should be translated to DE, FR, TR

Completely new:


Changed:

#napoveda_klavesy
#napoveda_prvky
#napoveda_indikatory
#m27


Deleted:
#zobr_konc_rad
-----------------------------------------------------------------------------


